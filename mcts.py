import copy
import gym
import numpy as np
import torch
import weakref

from gym_go import gogame
from game import GoState
from ann import create_nn_input_from_mcts_state_this_is_a_long_name



class NoneMCTStateSingleton:
    obj = None

    def get(self):
        return NoneMCTStateSingleton.obj

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (
                    self.obj == other.obj
            )
        else:
            return False

    def __len__(self):
        return 0


none = NoneMCTStateSingleton()


class MCTStateNode:
    def __init__(self, state, parent, c, policy, v):
        self.state = state
        self.parent = weakref.ref(parent)
        self.children = dict()
        self.reward = 0
        self.n = 0
        self.c = c

        self.policy = policy.squeeze()
        self.v = v.squeeze().item()

    def is_terminal(self):
        return self.state.end_state()

    def all_possible_moves(self):
        moves = []
        for p in np.argwhere(gogame.valid_moves(self.state.board) == 1):
            moves.append((p.item(), gogame.next_state(self.state.board, p.item())))

        return moves

    def get_child_index(self, obj):
        for c, i in self.children.items():
            if c == obj:
                return i

        return None

    def uct(self):
        policy = self.get_parent().policy[self.get_parent().get_child_index(self)].detach() if self.get_parent() else 1
        U = policy / (self.n + 1)
        Q = np.inf if self.n == 0 else np.sum([c.v for c in self.children]) / self.n
        return U + Q

    def backpropagate(self, reward):
        self.n = self.n + 1
        self.reward = self.reward + reward
        if self.get_parent():
            self.get_parent().backpropagate(-reward)

    def get_parent(self):
        if isinstance(self.parent(), NoneMCTStateSingleton):
            return None
        else:
            return self.parent()

    def current_player(self):
        return 1 if (self.get_parent().current_player() if self.get_parent() else 1) == -1 else -1

    def p1_winner(self):
        return self.state.p1_winner()

    def p2_winner(self):
        return self.state.p2_winner()

    def delete(self):
        for c in self.children:
            c.delete()

        for c in self.children:
            del c

        self.parent = None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (
                    self.state == other.state and
                    self.n == other.n and
                    self.get_parent() == other.get_parent() and
                    self.reward == other.reward and
                    self.c == other.c and
                    np.allclose(self.policy.detach(), other.policy.detach())
            )
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.state.board.tobytes(), self.get_parent()))


class MCTStateTree:
    def __init__(self, c, model, player):
        self.model = model
        self.player = player
        inputs = create_nn_input_from_mcts_state_this_is_a_long_name(None, self.player)
        self.root = MCTStateNode(GoState(gogame.init_state(6)), none, c, *model.forward(inputs))

    def selection(self):
        # 1. Find best possible leaf node
        current_node = self.root
        while len(current_node.children) > 0:
            current_node = list(current_node.children.keys())[
                np.argmax([c.uct() for c in current_node.children.keys()])]

        # 2. If not terminal node and has been visited, add all possible children and choose one
        if (not current_node.is_terminal()) and (current_node.n > 0):
            current_node.children = {
                MCTStateNode(
                    GoState(state),
                    current_node,
                    current_node.c,
                    *self.model.forward(
                        create_nn_input_from_mcts_state_this_is_a_long_name(current_node, (
                            1 if current_node.current_player() == -1 else -1))
                    )
                ): p
                for p, state in current_node.all_possible_moves()
            }

            # All newly added nodes have a UCT of infinite, so just pick one WRONG, ESTIMATES WITH NN
            current_node = list(current_node.children.keys())[0]

        # 3. Rollout from node
        # reward = self.rollout(current_node)
        reward = current_node.v
        # 4. Backpropagate results
        current_node.backpropagate(reward)

    def make_move(self):
        if len(self.root.children.keys()) > 0:
            next_state = list(self.root.children.keys())[np.argmax([c.n for c in self.root.children.keys()])]
            best = self.root.get_child_index(next_state)
            self.root = next_state
        else:
            best = gogame.random_action(self.root.state.board)
        return best, self.root

    def update_enemy_move(self, state):
        next_state = None
        for c in self.root.children:
            if np.all(c.state.board == state):
                next_state = c
        if next_state:
            self.root = next_state
        else:
            inputs = create_nn_input_from_mcts_state_this_is_a_long_name(None, self.player)
            self.root = MCTStateNode(GoState(state), none, self.root.c, *self.model.forward(inputs))
