import numpy as np
import torch
import torch.nn as nn



class AlphaGoModel(nn.Module):
    def __init__(self, config: dict):
        super(AlphaGoModel, self).__init__()
        self.grid_size = config["grid_size"]
        self.n_channels = 17
        self.n_res_blocks = 1
        self.hidden_layer = 32

        conv_layers = []
        # Convolutional block
        conv_layers.append(nn.Conv2d(self.n_channels, self.hidden_layer, (3, 3), stride = 1, padding=1))
        conv_layers.append(nn.BatchNorm2d(self.hidden_layer))
        conv_layers.append(nn.ReLU())

        self.conv_block = nn.Sequential(*conv_layers)

        self.res_layers = []
        # Residual blocks
        for i in range(self.n_res_blocks):
            res_layer = dict()
            res_layer["conv"] = nn.Conv2d(self.hidden_layer, self.hidden_layer, (3, 3), stride = 1, padding = 1)
            res_layer["batchnorm"] = nn.BatchNorm2d(self.hidden_layer)
            res_layer["relu"] = nn.ReLU()
            res_layer["conv2"] = nn.Conv2d(self.hidden_layer, self.hidden_layer, (3, 3), stride = 1, padding = 1)
            res_layer["batchnorm2"] = nn.BatchNorm2d(self.hidden_layer)
            res_layer["skip"] = nn.Identity()
            res_layer["relu2"] = nn.ReLU()
            self.res_layers.append(res_layer)

        self.policy_layers = []
        self.policy_layers.append(nn.Conv2d(self.hidden_layer, 2, (1, 1), stride = 1))
        self.policy_layers.append(nn.BatchNorm2d(2))
        self.policy_layers.append(nn.ReLU())
        self.policy_layers.append(nn.Linear(72, self.grid_size ** 2 + 1))
        self.policy_layers.append(nn.Softmax())

        self.policy_block = nn.Sequential(*self.policy_layers)

        self.value_layers = []
        self.value_layers.append(nn.Conv2d(self.hidden_layer, 1, (1, 1), stride = 1))
        self.value_layers.append(nn.BatchNorm2d(1))
        self.value_layers.append(nn.ReLU())
        self.value_layers.append(nn.Linear(36, self.hidden_layer))
        self.value_layers.append(nn.ReLU())
        self.value_layers.append(nn.Linear(self.hidden_layer, 1))
        self.value_layers.append(nn.Tanh())

        self.value_block = nn.Sequential(*self.value_layers)

        self.optimizer = torch.optim.Adam(params = self.parameters(), lr=0.1)


    def forward_res_tower(self, input):
        assert input.shape[3] == self.grid_size and input.shape[2] == self.grid_size and input.shape[1] == self.n_channels

        input_res_block = self.conv_block(input)

        for i in range(self.n_res_blocks):
            res_layer = self.res_layers[i]
            output_res_conv = res_layer["conv"](input_res_block)
            output_res_batchnorm = res_layer["batchnorm"](output_res_conv)
            output_res_relu = res_layer["relu"](output_res_batchnorm)
            output_res_conv2 = res_layer["conv2"](output_res_relu)
            output_res_batchnorm2 = res_layer["batchnorm2"](output_res_conv2)
            output_res_block = res_layer["relu2"](output_res_batchnorm2 + input_res_block)
            input_res_block = output_res_block

        return output_res_block

    def forward_policy(self, input):
        output_conv = self.policy_layers[0](input)
        output_batchnorm = self.policy_layers[1](output_conv)
        output_relu = self.policy_layers[2](output_batchnorm)
        output_view = output_relu.view(-1, 72)
        output_lin = self.policy_layers[3](output_view)
        return self.policy_layers[4](output_lin)

    def forward_value(self, input):
        output_conv = self.value_layers[0](input)
        output_batchnorm = self.value_layers[1](output_conv)
        output_relu = self.value_layers[2](output_batchnorm)
        output_view = output_relu.view(-1, 36)
        output_lin1 = self.value_layers[3](output_view)
        output_relu = self.value_layers[4](output_lin1)
        ouput_lin2 = self.value_layers[5](output_relu)
        return self.value_layers[6](ouput_lin2)

    def forward(self, input):
        output_res_block = self.forward_res_tower(torch.from_numpy(input).float())
        return self.forward_policy(output_res_block), self.forward_value(output_res_block)



def compute_loss(p, v, pi, z, model, c = 1e-4):
    # z - v ^2 = batch_size
    # p: batch_size, 37
    # pi: batch_size, 37
    # (batch_size, 37 * 37, batch_size)
    # log(p) * pi.T
    p = p + 1e-15
    loss = (z - v) ** 2 - torch.matmul(np.log(p), pi.T)
    l2_squared = 0
    for param in model.parameters():
        l2_squared += (param ** 2).sum()
    loss = loss.sum() + c * l2_squared
    return loss


def create_nn_input_from_mcts_state_this_is_a_long_name(board_state, player):
    zeros = np.zeros((16, 6, 6))
    indicator = np.ones((6, 6))

    if player == 1:  # white or black
        indicator = indicator * -1

    current_parent = board_state
    current_player = player

    for i in range(16):
        if not current_parent:
            break

        board = current_parent.state.board
        board = board[current_player]
        current_player = current_player * -1
        zeros[i] = board
        if i % 2 == 1:
            current_parent = current_parent.get_parent()

    return np.concatenate((np.flipud(zeros), indicator[np.newaxis, :]))[np.newaxis, :]
