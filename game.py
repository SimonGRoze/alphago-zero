import gym
import numpy as np
import torch

from abc import ABC, abstractmethod
from gym_go import gogame



class GameState(ABC):
    # Returns all possible moves from the current state. This
    # should return all moves for the CURRENT player - therefore it
    # might be useful to keep track of whose turn it is.

    # In testing (using tic-tac-toe), keeping track of the current
    # player was done using the number of empty spaces on the board
    # (assuming the player labelled 'X' always starts). This is not
    # possible in Go, as some moves remove an arbitrary number of
    # pieces from the board.
    @abstractmethod
    def all_possible_moves(self) -> list:
        raise NotImplementedError

    # Returns whether the current state is a terminal state.
    @abstractmethod
    def end_state(self) -> bool:
        raise NotImplementedError

    # Utility functions used to determine whether the algorithm is
    # winning against its opponent or not

    # Highly suggested to reuse this in the definition of end_state,
    # which is simply whether one of the two players is a winner, or
    # there is a tie (or some other terminal event)
    @abstractmethod
    def p1_winner(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def p2_winner(self) -> bool:
        raise NotImplementedError

    # Utility functions used to compare board states. The current
    # implementation should be enough, provided that the subclass
    # used has an internal variable called 'board', representing the
    # current board state (I recommend an np.ndarray).
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return np.array_equal(self.board, other.board)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)



class GoState(GameState):
    def __init__(self, board):
        self.komi = 1.5
        self.board = board

    def all_possible_moves(self):
        return gogame.children(self.board)

    def p1_winner(self):
        return gogame.game_ended(self.board) and gogame.winning(self.board, self.komi) == 1

    def p2_winner(self):
        return gogame.game_ended(self.board) and gogame.winning(self.board, self.komi) == -1

    def end_state(self):
        return gogame.game_ended(self.board)
