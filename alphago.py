import copy
import gc
import gym
import numpy as np
import pickle
import random
import torch
import wandb
import sys

from ann import AlphaGoModel, compute_loss, create_nn_input_from_mcts_state_this_is_a_long_name
from game import GoState
from mcts import MCTStateTree
from tqdm import tqdm



class GameHistory:
    def __init__(self):
        self.states = []
        self.winner = None
        self.best_mcts = None

    def add_state(self, state):
        self.states.append(state)

    def set_winner(self, who):
        self.winner = who

    def set_best_mcts(self, which):
        self.best_mcts = which



class GoBotAISkynetTropCool:
    # 0. Init NN
    # 1. Joue des parties (1600 MCTS / move ish)
    # 1. b) Sauvegarde position + résultats évaluation MCTS + résultats partie
    # 2. Après X parties, entraîne le NN
    # 3. Garde la meilleure IA
    # 4. Si une nouvelle IA gagne > 55% par rapport à la meilleure, nouvelle = meilleure
    # 4. b) À chaque 1000 itérations
    # 4.5 Rince and repeat 700 000 fois (sample + entraîne, tout en jouant)
    # 5. ???
    # 6. Profit

    def __init__(self, c):
        # OK DABORD
        self.c = c

        self.ann = AlphaGoModel({"grid_size": 6})
        self.best_ann = AlphaGoModel({"grid_size": 6})

        # self.mcts = MCTStateTree(c, self.ann)
        # self.best_mcts = MCTStateTree(c, self.best_ann)

        self.go_env = gym.make('gym_go:go-v0', size=6, komi=1.0)

        self.previous_games = []

    def play_game(self, render_mode=None):
        self.go_env.reset()

        game_history = GameHistory()
        player_best_mcts = np.random.randint(2)
        if player_best_mcts == 1:
            first_player = MCTStateTree(self.c, self.best_ann, 1)
            second_player = MCTStateTree(self.c, self.ann, -1)
        else:
            first_player = MCTStateTree(self.c, self.ann, 1)
            second_player = MCTStateTree(self.c, self.best_ann, -1)
        game_history.set_best_mcts(1 if player_best_mcts == 1 else -1)
        # Game loop
        done = False
        n_mcts = 100
        passed = 0
        while not done:
            # 1er joueur joue
            for _ in range(n_mcts):
                first_player.selection()
            first_player_action, current_state = first_player.make_move()
            game_history.add_state(current_state)
            state, reward, done, info = self.go_env.step(first_player_action)
            #print(state[4, 0, 0])
            passed = passed + 1 if state[4, 0, 0] else 0
            second_player.update_enemy_move(state)
            if passed > 1:
                print("Both players passed!")
                   
            if self.go_env.game_ended():
                break
            # 2e joueur joue
            for _ in range(n_mcts):
                second_player.selection()
            second_player_action, current_state = second_player.make_move()
            game_history.add_state(current_state)
            state, reward, done, info = self.go_env.step(second_player_action)
            first_player.update_enemy_move(state)
            passed = passed + 1 if state[4, 0, 0] else 0
            if passed > 1:
                print("Both players passed!")
            
            if self.go_env.game_ended():
                print("Game ended!")
                break
            if len(game_history.states) > 200:
                print("Too many turns!")
                break
        if render_mode:
            self.go_env.render(mode=render_mode)
        # go_env.winning returns the current winner
        game_history.set_winner(self.go_env.winning())
        return game_history

    def train_epoch(self, n_games, batch_size):
        previous_games = []
        for _ in range(n_games):
            print("Game")
            previous_games.append(self.play_game())
        #         with mp.Pool(processes=4) as pool:
        #             results = [pool.apply_async(self.playgame, args=(self)) for  in range(n_games)]
        #             previous_games = [g.get() for g in results]
        inputs_batch = []
        policy_batch = []
        value_batch = []
        for _ in range(batch_size):
            game = np.random.choice(previous_games)
            try:
                print(len(game.states))
                state = np.random.choice(game.states[1:])
            except:
                print(game.states)
            inputs_batch.append(create_nn_input_from_mcts_state_this_is_a_long_name(state, state.current_player()))
            children_policy = torch.nn.functional.softmax(torch.tensor([float(s.n) for s in state.children]))
            policy = torch.zeros(37)
            child_index = 0
            for move in state.children.values():
                policy[move] = children_policy[child_index]
                child_index += 1
            policy_batch.append(policy)
            value_batch.append(game.winner)
        pi, z = self.ann.forward(np.array(inputs_batch).squeeze())
        loss = compute_loss(torch.stack(policy_batch).squeeze(), torch.tensor(value_batch).squeeze(), pi,
                            z.squeeze(), self.ann)
        wandb.log({"loss": loss})
        print(loss)
        self.ann.optimizer.zero_grad()
        loss.backward()
        self.ann.optimizer.step()
        # We check if the current nn beats the best nn
        if np.sum(np.array([game.winner != game.best_mcts for game in previous_games])) / len(
                previous_games) > 0.55:
            self.best_ann = copy.deepcopy(self.ann)
            print("Updated best player")



seed = 0
torch.manual_seed(seed)
random.seed(seed)
np.random.seed(seed)
#torch.use_deterministic_algorithms(True if seed else False)
n_epoch = 2
n_games = 1
batch_size = 10
c = 2

run = wandb.init(project="alphago")
run.config.n_epoch = n_epoch
run.config.n_games = n_games
run.config.batch_size = batch_size
run.config.c = c

alpha_go = GoBotAISkynetTropCool(c)

for i in tqdm(range(n_epoch)):
    alpha_go.train_epoch(n_games, batch_size)

with open("alphago" + sys.argv[1] + ".pkl", "wb") as f: 
    pickle.dump(alpha_go, f)